	{{-- footer section --}}
    <footer class="ftco-footer ftco-section">
        <div class="container">
          <div class="row mb-5">
            <div class="col-md">
              <div class="ftco-footer-widget mb-4">
                <h2 class="ftco-heading-2">{{config('app.name')}}</h2>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                  <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                  <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                  <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                </ul>
              </div>
            </div>
            <div class="col-md">
              <div class="ftco-footer-widget mb-4 ml-md-4">
                <h2 class="ftco-heading-2">Links</h2>
                <ul class="list-unstyled">
                  <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Home</a></li>
                  <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>About</a></li>
                  <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Services</a></li>
                  <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Projects</a></li>
                  <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Contact</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md">
               <div class="ftco-footer-widget mb-4">
                <h2 class="ftco-heading-2">Services</h2>
                <ul class="list-unstyled">
                  <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Architectural Design</a></li>
                  <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Interior Design</a></li>
                  <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Exterior Design</a></li>
                  <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>Lighting Design</a></li>
                  <li><a href="#"><span class="icon-long-arrow-right mr-2"></span>AutoCAD Service</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md">
                  <div class="ftco-footer-widget mb-4">
                      <h2 class="ftco-heading-2">Have a Questions?</h2>
                      <div class="block-23 mb-3">
                      <ul>
                          <li><span class="icon icon-map-marker"></span><span class="text">Imperial constructions 99/1 galthotamulla yakkla</span></li>
                          <li><a href="#"><span class="icon icon-phone"></span><span class="text">+94 71 65 26 452</span></a></li>
                          <li><a href="#"><span class="icon icon-envelope"></span><span class="text">aselakumara22@gmail.com</span></a></li>
                      </ul>
                      </div>
                  </div>
              </div>
              </div>
              <div class="row">
              <div class="col-md-12 text-center">

                      <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
          Copyright &copy;{{$data_aray['thisyear']}} | All rights reserved |  This site is developed with <i class="fas fa-heart" aria-hidden="true"></i> by <a href="https://www.facebook.com/theekshana.rocksreeper" target="_blank">Nipuna Theekshana</a>
          <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                  </div>
                  </div>
              </div>
      </footer>
