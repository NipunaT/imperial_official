<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
      <a class="navbar-brand" href="/"><span>{{config('app.name')}}.</span></a>
      <button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

      <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav nav ml-auto">
          <li class="nav-item"><a href="#" class="nav-link" data-nav-section="home"><span>Home</span></a></li>
          <li class="nav-item"><a href="#" class="nav-link" data-nav-section="about"><span>About</span></a></li>
          <li class="nav-item"><a href="#" class="nav-link" data-nav-section="team"><span>Gallary</span></a></li>
          <li class="nav-item"><a href="#" class="nav-link" data-nav-section="testimony"><span>Our Team</span></a></li>
          <li class="nav-item"><a href="#" class="nav-link" data-nav-section="blog"><span>Projects</span></a></li>
          <li class="nav-item"><a href="#" class="nav-link" data-nav-section="contact"><span>Contact</span></a></li>
          <li class="nav-item cta"><a href="#" class="nav-link">Get a quote </a></li>

        </ul>
      </div>
    </div>
</nav>
