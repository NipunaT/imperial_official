@extends('admin.app')
@section('title', 'Project Upload')
@section('content')




    <legend class="uk-legend uk-text-center uk-margin-large-top">Project Create</legend>

    <div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin uk-width-5-6 uk-align-center" style="border-radius: 12px" >

        @if (count($errors) > 0)
            <div class="uk-alert-danger" uk-alert>
                <a class="uk-alert-close" uk-close></a>
                <strong>Sorry!</strong> There are some problems in your project.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        @if(session('success'))
            <div class="uk-alert-success" uk-alert>
                <a class="uk-alert-close" uk-close></a>
                {{ session('success') }}
            </div>
        @endif



        <form method="post" action="/upload_project" enctype="multipart/form-data">
            {{csrf_field()}}

            <div class="uk-margin">
                <span class="uk-label" style="background-color: #2e3e4f ; border-radius: 12px">Project name</span>
                <input class="uk-input"  name="Project_name" id="txt" type="text" autocomplete="off" placeholder="Project name">
            </div>

            <div class="uk-margin">
                <span class="uk-label" style="background-color: #2e3e4f ; border-radius: 12px">Location</span>
                <input class="uk-input" id="txt" name="location" type="text" autocomplete="off" placeholder="Location">
            </div>

            <div class="uk-margin">
                <span class="uk-label" style="background-color: #2e3e4f ; border-radius: 12px">owner</span>
                <input class="uk-input" name="owner" id="txt" type="text" autocomplete="off" placeholder="owner">
            </div>

            <div class="uk-margin">
                <span class="uk-label" style="background-color: #2e3e4f ; border-radius: 12px">date started</span>
                <input class="uk-input" name="date_started" id="txt" type="date" autocomplete="off" placeholder="date started">
            </div>

            <div class="uk-margin">
                <span class="uk-label" style="background-color: #2e3e4f ; border-radius: 12px">date compleated</span>
                <input class="uk-input" name="date_compleated" id="txt" type="date" autocomplete="off" placeholder="date compleated">
            </div>

            <div class="uk-margin">
                <textarea class="uk-textarea" rows="5" name="disciption" id="txt" autocomplete="off" placeholder="Disciption"></textarea>
            </div>

            <div class="input-group hdtuto control-group lst uk-child-width-1-1@m" uk-grid>

                <div class="uk-width-expand">
                    <input class="uk-input" id="file" type="file" name="images[]" class="myfrm form-control">
                </div >

                <div class="input-group-btn uk-width-1-6 uk-animation-toggle" tabindex="0">
                    <i class="fas fa-plus btn-success uk-text-primary uk-animation-scale-down" style="font-size: large;margin-top:8px" uk-tooltip='Add' ></i>
                </div>

            </div>

            <div class="clone uk-hidden increment">
                <div class="hdtuto control-group lst input-group uk-child-width-1-2@m" style="margin-top:10px" uk-grid>

                <div class="uk-width-expand">
                    <input class="uk-input" id="file" type="file" name="images[]" class="myfrm form-control">
                </div>

                <div class="input-group-btn uk-width-1-6 uk-animation-toggle" tabindex="0">
                    <i class="fas fa-times btn-danger uk-text-danger uk-animation-shake"    style="font-size: large;margin-top:8px" uk-tooltip='Remove' ></i>
                </div>

                </div>
            </div>


            <div class="uk-margin">
                <button type="submit" class="uk-button uk-button-primary uk-width-1-1" id="upload_button" >Upload<i
                    class="fa fa-upload uk-margin-small-left"></i></button>
            </div>

        </form>
    </div>






    <script type="text/javascript">
        $(document).ready(function() {
        $(".btn-success").click(function(){
            var lsthmtl = $(".clone").html();
            $(".increment").after(lsthmtl);
        });
        $("body").on("click",".btn-danger",function(){
            $(this).parent().parent().remove();
        });
        });

    </script>
@endsection
