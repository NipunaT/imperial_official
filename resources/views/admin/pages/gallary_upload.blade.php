@extends('admin.app')
@section('title', 'gallary upload')
@section('content')

<legend class="uk-legend uk-text-center uk-margin-large-top">Upload gallary</legend>


<div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin uk-width-5-6 uk-align-center" style="border-radius: 12px" >

    <form method="post" action="/upload_gallary" enctype="multipart/form-data">

        @csrf

        <div class="uk-margin">
            <span class="uk-label" style="background-color: #2e3e4f ; border-radius: 12px">Image name name</span>
            <input class="uk-input"  name="image_name" id="txt" type="text" autocomplete="off" placeholder="Image name">
        </div>

        <div class="uk-margin">
            <span class="uk-label" style="background-color: #2e3e4f ; border-radius: 12px">Project name</span>
            <input class="uk-input"  name="project_name" id="txt" type="text" autocomplete="off" placeholder="project name">
        </div>

        <div class="uk-margin">
            <span class="uk-label" style="background-color: #2e3e4f ; border-radius: 12px">Location</span>
            <input class="uk-input"  name="location" id="txt" type="text" autocomplete="off" placeholder="Location taken">
        </div>

        <div class="uk-margin">
            <textarea class="uk-textarea" rows="5" name="disciption" id="txt" autocomplete="off" placeholder="Disciption"></textarea>
        </div>




        <button type="submit" class="uk-button uk-button-primary uk-width-1-1" id="upload_button">Upload<i
            class="fa fa-upload uk-margin-small-left"></i></button>


    </form>







</div>










@endsection
