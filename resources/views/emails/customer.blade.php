@component('mail::message')
# Introduction

The body of your message.

{{$name}}
{{$email}}
{{$subject}}
{{$message}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
