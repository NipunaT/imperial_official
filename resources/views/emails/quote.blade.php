@component('mail::message')
# Introduction

The body of your message.

{{$message}}
{{$phone}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
