{{-- about section --}}
<section class="ftco-counter img ftco-section ftco-no-pt ftco-no-pb" id="section-counter" data-section="about">
    <div class="container">
        <div class="row d-flex">
            <div class="col-md-6 col-lg-4 d-flex">
                <div class="img d-flex align-self-stretch align-items-center" style="background-image:url(images/about.jpg);">
                    <div class="request-quote py-5">
                        <div class="py-2">
                            <span class="subheading">Be Part of our Business</span>
                            <h3>Request A Quote</h3>
                        </div>
                        <form  class="request-form ftco-animate" id="quote_message">
                            <div class="form-group">
                                <input  type="text" class="form-control" id="fname" placeholder="First Name">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="lname" placeholder="Last Name">
                            </div>
                            <div class="form-group">
                                <div class="form-field">
                                    <div class="select-wrap">
                                        <div class="icon-arr"><span class="ion-ios-arrow-down"></span></div>
                                        <select name="service" id="service" class="form-control">
                                                <option value="1">Select Your Services</option>
                                                <option value="2">Construction</option>
                                                <option value="3">Renovation</option>
                                                <option value="4">Interior Design</option>
                                                <option value="5">Exterior Design</option>
                                                <option value="6">Painting</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="phone" placeholder="Phone">
                            </div>
                            <div class="form-group">
                      <textarea name="" id="message" cols="30" rows="2" class="form-control" placeholder="Message"></textarea>
                    </div>
                    <div class="form-group">
                      <input type="submit" value="Request A Quote" class="btn btn-secondary py-3 px-4">
                    </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-8 pl-lg-5 py-5">
                <div class="row justify-content-start pb-3">
              <div class="col-md-12 heading-section ftco-animate">
                  <span class="subheading">Welcome</span>
                <h2 class="mb-4">Since we started work in 2002</h2>
                <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>
                <p>A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
              </div>
            </div>
                <div class="row">
              <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate d-flex">
                <div class="block-18 text-center p-4 mb-4 align-self-stretch d-flex">
                  <div class="text">
                    <strong class="number" data-number="{{$data_aray['years']}}">0</strong>
                    <span>Years of experience</span>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate d-flex">
                <div class="block-18 text-center py-4 px-3 mb-4 align-self-stretch d-flex">
                  <div class="text">
                    <strong class="number" data-number="{{$data_aray['projects_done']}}">0</strong>
                    <span>Project Done</span>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate d-flex">
                <div class="block-18 text-center py-4 px-3 mb-4 align-self-stretch d-flex">
                  <div class="text">
                    <strong class="number" data-number="{{$data_aray['house_plans']}}">0</strong>
                    <span>House Plans</span>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-lg-3 justify-content-center counter-wrap ftco-animate d-flex">
                <div class="block-18 text-center py-4 px-3 mb-4 align-self-stretch d-flex">
                  <div class="text">
                    <strong class="number" data-number="{{$data_aray['coustomers']}}">0</strong>
                    <span>Happy Customers</span>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
    </div>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"></script>

<script>

    $('#quote_message').on('submit',function(event){
        event.preventDefault();

        first_name = $('#fname').val();
        last_name = $('#lname').val();
        service = $('#service').val();
        phone = $('#phone').val();
        message = $('#message').val();


        $.ajax({
        url: "/quote_message",
        type:"get",
        data:{
            "_token": "{{csrf_token()}}",
            first_name:first_name,
            last_name:last_name,
            service:service,
            phone:phone,
            message:message,
            },
            success:function(res){

                if(res.status=='success'){

                    iziToast.success({
                    title: res.msg,
                    position: 'topRight',
                    timeout: 2000

                    });

                }
                else{

                    iziToast.warning({
                    title: res.msg,
                    position: 'topRight',
                    timeout: 4000

                    });
                }


            },

        });

    });

</script>
