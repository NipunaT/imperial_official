{{-- home background image --}}
<section class="hero-wrap js-fullheight" style="background-image: url('images/bg_1.jpg');" data-section="home">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
        <div class="col-md-8 ftco-animate mt-5" data-scrollax=" properties: { translateY: '70%' }">
            {{-- <p class="d-flex align-items-center" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">
                          <a href="https://vimeo.com/45830194" class="icon-video popup-vimeo d-flex justify-content-center align-items-center mr-3">
                          <span class="ion-ios-play play mr-2"></span>
                          <span class="watch">Watch Video</span>
                      </a>
                      </p> --}}
          <h1 class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">We have a passion in creating new and unique HOME DESIGNS</h1>
          <p class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }">we make your dream happen</p>
        </div>
      </div>
    </div>
  </section> 