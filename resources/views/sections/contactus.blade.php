{{-- contactus form section --}}
<section class="ftco-section contact-section ftco-no-pb" data-section="contact">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
        <div class="col-md-7 heading-section text-center ftco-animate">
          <span class="subheading">Contact</span>
          <h2 class="mb-4">Contact Us</h2>
          <p>You can reach us 24/7 as bellow</p>
        </div>
      </div>
      <div class="row no-gutters block-9">
        <div class="col-md-6 order-md-last d-flex">
          <form  class="bg-light p-5 contact-form" id="contactForm">
            <div class="form-group">
              <input name="name" id="name" type="text" class="form-control" placeholder="Your Name">
            </div>
            <div class="form-group">
              <input name="email" id="email" type="text" class="form-control" placeholder="Your Email">
            </div>
            <div class="form-group">
              <input name="subject" id="subject" type="text" class="form-control" placeholder="Subject">
            </div>
            <div class="form-group">
              <textarea name="message" id="message" name="" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
            </div>
            <div class="form-group">
              <input type="submit" value="Send Message" class="btn btn-secondary py-3 px-5">
            </div>
          </form>

        </div>

        <div class="col-md-6 d-flex">
          <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3959.310911136329!2d80.0339619153261!3d7.089912118284931!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ae2fd412112cf1d%3A0xbb97871e13748dd7!2sMaheefoods.lk!5e0!3m2!1sen!2sus!4v1591074754759!5m2!1sen!2sus"
          width="100%" height="100%" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
      </div>
    </div>
  </section>
  {{-- contact us detailed section --}}
  <section class="ftco-section contact-section">
    <div class="container">
      <div class="row d-flex contact-info">
        <div class="col-md-6 col-lg-3 d-flex">
            <div class="align-self-stretch box p-4 text-center">
                <div class="icon d-flex align-items-center justify-content-center">
                    <span class="icon-map-signs"></span>
                </div>
                <h3 class="mb-4">Address</h3>
              <p>Imperial constructions 99/1 galthotamulla yakkla</p>
            </div>
        </div>
        <div class="col-md-6 col-lg-3 d-flex">
            <div class="align-self-stretch box p-4 text-center">
                <div class="icon d-flex align-items-center justify-content-center">
                    <span class="icon-phone2"></span>
                </div>
                <h3 class="mb-4">Contact Number</h3>
              <p><a href="tel://1234567920">+94 71 65 26 452 </a></p>
            </div>
        </div>
        <div class="col-md-6 col-lg-3 d-flex">
            <div class="align-self-stretch box p-4 text-center">
                <div class="icon d-flex align-items-center justify-content-center">
                    <span class="icon-paper-plane"></span>
                </div>
                <h3 class="mb-4">Email Address</h3>
              <p><a href="mailto:info@yoursite.com">aselakumara22@gmail.com</a></p>
            </div>
        </div>
        <div class="col-md-6 col-lg-3 d-flex">
            <div class="align-self-stretch box p-4 text-center">
                <div class="icon d-flex align-items-center justify-content-center">
                    <span class="icon-globe"></span>
                </div>
                <h3 class="mb-4">Website</h3>
              <p><a href="#">imperialconstruction.lk</a></p>
            </div>
        </div>
      </div>
    </div>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"></script>

<script>

    $('#contactForm').on('submit',function(event){
        event.preventDefault();

        name = $('#name').val();
        email = $('#email').val();
        subject = $('#subject').val();
        message = $('#message').val();


        $.ajax({
          url: "/message",
          type:"get",
          data:{
            "_token": "{{csrf_token()}}",
            name:name,
            email:email,
            subject:subject,
            message:message,
            },
            success:function(res){

                if(res.status=='success'){

                    iziToast.success({
                    title: res.msg,
                    position: 'topRight',
                    timeout: 2000

                    });


                }
                else{

                    iziToast.warning({
                    title: res.msg,
                    position: 'topRight',
                    timeout: 4000

                    });
                }


            },

        });

    });


</script>
