{{-- blog section --}}
<section class="ftco-section bg-light" data-section="blog">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <span class="subheading">Blog</span>
                <h2 class="mb-4">Read Our Stories</h2>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
            </div>
        </div>
        <div class="row d-flex">
                {{-- projects bigin --}}

                @foreach($data_aray['projects'] as $project)

                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="blog-entry">
                            <a onclick="data_cart('{{$project->id}}')" class="block-20" style="background-image: url('{{$project->url}}');"></a>
                            <div class="text mt-3 float-right d-block">
                                <div class="d-flex align-items-center pt-2 mb-4 topp">
                                    <div class="one mr-3">
                                    <span class="day"></span>
                                    </div>
                                    <div class="two">
                                        <span class="yr">2019</span>
                                        <span class="mos">March</span>
                                    </div>
                                </div>
                                <h3 class="heading"><a href="single.html">{{$project->name}}</a></h3>

                                <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                                <div class="d-flex align-items-center mt-4 meta">
                                <p class="mb-0"><button href="#" onclick="data_cart('{{$project->id}}')" class="btn btn-secondary">View pictures <span class="ion-ios-arrow-round-forward"></span></button></p>

                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach

                {{-- projects end --}}
        </div>
    </div>
</section>

{{-- modal --}}

<div id="modal-full" class="uk-modal-full" uk-modal>
    <div class="uk-modal-dialog  uk-margin-auto-vertical uk-background-muted ">
        <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
        {{-- content --}}

        <div class="uk-child-width-1-3@m project_img" uk-grid uk-lightbox="animation: slide"  autoplay='true' autoplay-interval='4000'>

                {{-- images appending location --}}

        </div>

        {{-- content end --}}
    </div>
</div>
<Script>
    // projectimage data ajax
    function data_cart(id){


        var data = {"_token": "{{csrf_token()}}"}
        var request = $.ajax({
            url: "/project/"+id+"",
            type: "get",
            data: data,
            dataType: "json",

            success: function (result) {
                ;
                //clear the previous apended data
                $('#modal-full .project_img').empty();

                //appending new data
                $.each(result, function( index, value ) {

                    $('#modal-full .project_img').append(
                        `
                        <div>
                            <a class="uk-inline" href="`+value.url+`" data-caption="`+value.discription+`">
                                <img src="`+value.url+`" alt="">
                            </a>
                        </div>
                        `
                    );

                });
                UIkit.modal("#modal-full").show();
            },
        });


    }

</Script>
