{{-- design and planing detail dection	 --}}
<section class="ftco-section ftco-services ftco-no-pt">
    <div class="container">
        <div class="row services-section">
        <div class="col-md-4 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services text-center d-block">
            <div class="icon"><span class="flaticon-layers"></span></div>
            <div class="media-body">
                <h3 class="heading mb-3">Perfectly Design</h3>
                <p>Our designers do their best to design your dream within the guidlines of municipal council and all the Astrologycal rules will be maintained properly.</p>
                <p><a href="#" class="btn btn-primary">Read more</a></p>
            </div>
            </div>      
        </div>
        <div class="col-md-4 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services text-center d-block">
            <div class="icon"><span class="flaticon-compass-symbol"></span></div>
            <div class="media-body">
                <h3 class="heading mb-3">Carefully Planned</h3>
                <p>withe the guidance of {{$data_aray['years']}} years of experiance your dream desing will be planed to pass the draft in one try from the municipal council.</p>
                <p><a href="#" class="btn btn-primary">Read more</a></p>
            </div>
            </div>    
        </div>
        <div class="col-md-4 d-flex align-self-stretch ftco-animate">
            <div class="media block-6 services text-center d-block">
            <div class="icon"><span class="flaticon-layers"></span></div>
            <div class="media-body">
                <h3 class="heading mb-3">Smartly Execute</h3>
                <p>Our enginears and hard working builders will bring your perfectly designed and well pland draft to life with in the fastest time posible .</p>
                <p><a href="#" class="btn btn-primary">Read more</a></p>
            </div>
            </div>      
        </div>
        </div>
    </div>
</section>