{{-- gallary section --}}
<section class="ftco-section" data-section="team">
    <div class="container-fluid p-0">
        <div class="row no-gutters justify-content-center pb-5">
            <div class="col-md-12 heading-section text-center ftco-animate">
                <span class="subheading">GALLARY</span>
                <h2 class="mb-4">Checkout Our Randomness</h2>
                <p>Along the way of {{$data_aray['years']}} years of E X P E R I A N S</p>
            </div>
        </div>
        <div class="row no-gutters uk-child-width-1-4@m">

            @foreach ($data_aray['gallary_photos'] as $gallary_photo)
            <div class="uk-text-center">
                <div class="uk-inline-clip uk-transition-toggle uk-light" tabindex="0">
                    {{-- <img height="500" src="{{asset($gallary_photo->url)}}" alt=""> --}}
                    <img height="500" class="img align-self-stretch" src="{{asset($gallary_photo->url)}}">

                    <div class="uk-position-center">
                        <div class="uk-transition-slide-top-small"><h4 class="uk-margin-remove uk-text-success" >{{$gallary_photo->project_name}}</h4></div>
                        <div class="uk-transition-slide-bottom-small"><h4 class="uk-margin-remove"><span uk-icon="location"></span> {{$gallary_photo->location}}</h4></div>
                    </div>
                </div>
            </div>

                {{-- <div class="col-md-6 col-lg-3 ftco-animate text-center">
                    <div class="staff">
                        <div class="img-wrap d-flex align-items-stretch">
                            <div >
                                <img height="500" class="img align-self-stretch" src="{{asset($gallary_photo->url)}}">
                            </div>
                        </div>
                        <div class="text  align-items-center pt-3"> 
                            <div>
                                <span class="position mb-2">{{$gallary_photo->location}}</span>
                                <h3 class="mb-4">{{$gallary_photo->project_name}}</h3>
                                <div class="faded " >
                                    <p>{{$gallary_photo->discription}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}

            @endforeach
                    
        </div>
    </div>
</section>