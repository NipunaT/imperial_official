{{-- designers and owner section --}}
<section class="" data-section="testimony">
    <div class="uk-container">
      <div class="row ftco-animate justify-content-center">
          <div class="col-md-12 d-flex align-items-center">
              <div class="carousel-testimony owl-carousel">

                  <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s uk-margin" uk-grid>
                    <div class="uk-card-media-left uk-cover-container" >
                        <img src="images/testimony-1.jpg" alt="" uk-img>
                    </div>
                    <div>
                        <div class="uk-card-body">


                        <span class="icon-quote-left"></span>

                          <p >Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                          <p >Jacob Bolton</p>
                          <span >CEO, Founder</span>

                        </div>
                    </div>
                </div>

              </div>
          </div>
      </div>
    </div>
</section>
