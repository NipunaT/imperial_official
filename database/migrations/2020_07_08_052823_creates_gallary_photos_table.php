<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatesGallaryPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallary_photos', function (Blueprint $table) {
            $table->id();
            $table->string('image_name');
            $table->string('url');
            $table->string('project_name');
            $table->string('discription')->nullable();
            $table->string('location')->nullable();
            $table->integer('Number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gallary_photos');
    }
}
