<?php

use Illuminate\Support\Facades\Route;



route::get('/','appcontroller@index');
route::get('/gallary','gallarycontroller@index');
route::get('/project/{id}','appcontroller@project');
route::get('/message','maillcontroller@message');
route::get('/quote_message','maillcontroller@quote');




//admin routes
Route::get('/project_upload', function () {
    return view('admin.pages.Project_upload');
});
Route::get('/gallary_upload', function () {
    return view('admin.pages.gallary_upload');
});

route::post('/upload_project','admincontroller@project_upload');
route::post('/upload_gallary','admincontroller@gallary_upload');








