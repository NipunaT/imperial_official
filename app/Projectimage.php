<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projectimage extends Model
{
    protected $guarded=[];

    public function project(){

        return $this->belongsTo('App\Project');

    }
}
