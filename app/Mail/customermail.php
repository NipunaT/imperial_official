<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class customermail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $data;

    public function __construct($data_aray)
    {
        $this->data=$data_aray;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $x=$this->data;

        return $this->markdown('emails.customer')
                    ->with([
                        'name' => $x['name'],
                        'email' => $x['email'],
                        'subject' => $x['subject'],
                        'message' => $x['message'],
                    ]);
    }
}
