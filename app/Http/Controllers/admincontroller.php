<?php

namespace App\Http\Controllers;

use App\Project;
use App\Projectimage;
use Illuminate\Http\Request;


class admincontroller extends Controller
{
    public function project_upload(Request $request){

        $this->validate($request, [
            'Project_name'=>'required',
            'disciption' => 'required',
            'location' => 'required',
            'owner' => 'required',
            'date_started' => 'required|date',
            'date_compleated' => 'required|date',
            'images' => 'required',
            'images.*' => 'mimes:jpeg,bmp,png'
        ]);


        //check if files available to upload
        if($request->hasfile('images'))
        {
            //create new project
            $project= new Project();
            $project->name=$request->Project_name;
            $project->disciption=$request->disciption;
            $project->location=$request->location;
            $project->owner=$request->owner;
            $project->date_started=$request->date_started;
            $project->date_compleated=$request->date_compleated;


            $image_id=0;
            $projectname=$request->Project_name;
            $x=0;

            //uploading each file
            foreach($request->file('images') as $file)
            {

                //creating image name
                $const='img-';
                $image_name =$const.$image_id;
                $guessExtension = $file->guessExtension();

                //folder name
                $foldername=$projectname;

                //uploading the image
                $file->storeAs("".$foldername."", $image_name.'.'.$guessExtension,'public_uploads' ); //public uload is the disk driver it is defined at config.filesystems


                //build url for the image
                $const_url="uploads/$projectname/";
                $url=$const_url.$image_name.'.'.$guessExtension;

                //adding the first image as project cover
                while($x==0){

                    $project->url=$url;
                    $project->save();
                    $x=$x+1;

                }


                //adding project images with laravel model relationships
                $projectimages=new Projectimage(['url' => $url]);
                $project->projectimages()->save($projectimages);

                $image_id=$image_id+1;
            }

        }






        return back()->with('success', 'Project created successfully');
    }
    public function gallary_upload(){


        
    }

}
