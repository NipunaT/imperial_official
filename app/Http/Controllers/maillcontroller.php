<?php

namespace App\Http\Controllers;

use App\Mail\ContactFormmail;
use App\Mail\customermail;
use App\Mail\quotemail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class maillcontroller extends Controller
{
    public function message(Request $request){


        //validate all the inputs
        $validator=Validator::make($request->all(),[
        'name'=>'required',
        'email'=>'email|required',
        'subject'=>'required',

        ]);

        //if validation fails
        if ($validator->fails()) {
            $ok = array(
                'status' => 'fail',
                'msg' => $validator->errors()->first()
            );
        }

        // if validation passes
        else{

            $email='testa@gmail.com';

            $data=[
                'name'=>$request->name,
                'email'=>$request->email,
                'subject'=>$request->subject,
                'message'=>$request->message
            ];

            Mail::to($email)->send(new ContactFormmail($data));

            Mail::to($request->email)->send(new customermail($data));

            $ok = array(
                'status' => 'success',
                'msg' => 'Mesege sent, will contact you soon',
            );

        }





        return response()->json( $ok );

    }

    public function quote(Request $request){

        //validate all the inputs
        $validator=Validator::make($request->all(),[
        'first_name'=>'required',
        'last_name'=>'required',
        'service'=>'required',
        'phone'=>'required|phone:LK',   //composer require propaganistas/laravel-phone  (phone number validating laravel package)
        'message'=>'required'
        ]);

        //if validation fails
        if ($validator->fails()) {

            //phon validation maessage
            if($validator->errors()->first()=='validation.phone'){

                $ok = array(
                    'status' => 'fail',
                    'msg' => 'Enter valid phone number'
                );
            }

            //validation errors
            else{

                $ok = array(
                    'status' => 'fail',
                    'msg' => $validator->errors()->first()
                );
            }

        }

        // if validation passes
        else{

            $email='testa@gmail.com';

            $data=[
                'first_name'=>$request->first_name,
                'last_name'=>$request->last_name,
                'service'=>$request->service,
                'phone'=>$request->phone,
                'message'=>$request->message
            ];

            $email='tedt@test.com';

            Mail::to($email)->send(new quotemail($data));

            $ok = array(
                'status' => 'success',
                'msg' => 'Request sent, will contact you soon',
            );

        }

        return response()->json( $ok );

    }
}
