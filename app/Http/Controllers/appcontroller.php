<?php

namespace App\Http\Controllers;

use App\Gallary_photo;
use App\Project;
use App\Projectimage;
use Illuminate\Http\Request;
use Carbon\Carbon;


class appcontroller extends Controller
{
    public function index(){

        //getting the curent year
        $now = Carbon::now();
        $thisyear=$now->format('Y');

        //pushing variables
        $projects_done=52;
        $house_plans=454;
        $coustomers=900;
        $years=$thisyear-2002;                  //callculate years of experiance
        $gallary_photos=Gallary_photo::all();  //getting gallary image data
        $projects=Project::all();


        $data_aray=['projects_done'=>$projects_done,
                    'house_plans'=>$house_plans,
                    'coustomers'=>$coustomers,
                    'years'=>$years,
                    'gallary_photos'=>$gallary_photos,
                    'projects'=>$projects,
                    'thisyear'=>$thisyear

                    ];


    //    app('App\Http\Controllers\gallarycontroller')->index();



        return view('app',compact('data_aray'));
    }

    public function project($id){

           $images=Projectimage::where('project_id','=',$id)->get();

           return response()->json( $images);
    }
}
